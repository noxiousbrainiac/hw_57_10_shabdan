import React, {useState} from 'react';
import EquallyCounter from "./EquallyCounter/EquallyCounter";

const EquallyPay = () => {
    const [order, setOrder] = useState({
        people: '',
        sum: ''
    });

    const changePeople = (people, value) => {
        setOrder(order => {
            return {
                ...order,
                people: +value
            };
        });
    };

    const changeSum = (sum, value) => {
        setOrder(order => {
            return {
                ...order,
                sum: +value
            };
        });
    };

    return (
        <div>
            <h2>Equally pay</h2>
            <form>
                <div className="form-group" style={{width: '400px'}}>
                    <label>Number of people</label>
                    <input
                        type="number"
                        className="form-control"
                        value={order.people}
                        onChange={e => changePeople('people', e.target.value)}
                    />
                </div>
                <div className="form-group" style={{width: '400px'}}>
                    <label>Order amount</label>
                    <input
                        type="number"
                        className="form-control"
                        value={order.sum}
                        onChange={e => changeSum('sum', e.target.value)}
                    />
                </div>
                <EquallyCounter order={order}/>
            </form>
        </div>
    );
};

export default EquallyPay;