import React, {useState} from 'react';
import '../../../assets/bootstrap.min.css';

const IndividualCounter = ({people}) => {
    const [total, setTotal] = useState(0);

    const [extraPay, setExtraPay] = useState({
        tips: '',
        delivery: ''
    });

    const tipsChange = (tips, value) => {
        setExtraPay(extraPay => {
            return {
                ...extraPay,
                tips: +value,
            };
        });
    };

    const deliveryChange = (delivery, value) => {
        setExtraPay(extraPay => {
            return {
                ...extraPay,
                delivery: +value,
            };
        });
    };

    const count = () => {
        setTotal(total => total - total);

        people.map(person => {
            return setTotal(total => total + person.howMuch);
        });

        if (extraPay.tips > 0){
            setTotal(total => total + (total * extraPay.tips)/100);
        }

        if (extraPay.delivery > 0){
            setTotal(total => total + extraPay.delivery);
        }
    };

    return (
        <div>
            <div style={{width: "400px"}}>
                <div className="form-group">
                    <label>Tips (%)</label>
                    <input
                        className="form-control my-1"
                        type="number"
                        value={extraPay.tips}
                        onChange={e => tipsChange('tips', e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label>Delivery (som)</label>
                    <input
                        className="form-control my-1"
                        type="number"
                        value={extraPay.delivery}
                        onChange={e => deliveryChange('delivery', e.target.value)}
                    />
                </div>
            </div>
            <div>
                <button type="button" className="btn btn-primary m-2" onClick={count}>Count</button>
            </div>
            {total > 0 ? (
                <div>
                    <p className="m-1">Total price: <b onChange={total}>{Math.ceil(total)}</b> som</p>
                    {people.map(person => {
                        const personPays = person.howMuch + (person.howMuch * extraPay.tips)/ 100 + (extraPay.delivery / people.length);

                        return (
                            <p
                                key={person.id}
                                className="m-1"
                            >
                                {person.name}: <b>{Math.ceil(personPays)}</b> som
                            </p>
                        )
                    })}
                </div>
            ) : (
                <p>Nothing counted</p>
            )}
        </div>
    );
};

export default IndividualCounter;