import React, {useState} from 'react';
import {nanoid} from "nanoid";
import IndividualCounter from "./IndividualCounter/IndividualCounter";

const IndividualPay = () => {
    const [people, setPeople] = useState([]);

    const addPerson = () => {
        setPeople(people => [
            ...people,
            {name: '', howMuch: '',  id: nanoid()}
        ]);
    };

    const removePerson = id => {
        setPeople(people.filter(person => person.id !== id));
    };

    const changePersonField = (id, name, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, [name]: value}
                }

                return person;
            });
        });
    };

    const changePersonPay = (id, howMuch, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, howMuch: +value}
                }

                return person;
            });
        });
    };

    return (
        <div>
            <h2>Individual pay</h2>
            <form>
                <button
                    className="btn btn-primary"
                    type="button"
                    onClick={addPerson}
                >
                    Add person
                </button>
                <div className="p-4 m-4">
                    {people.length > 0 ? (
                        people.map(person => (
                            <div
                                className="d-flex p-2"
                                key={person.id}
                            >
                                <div>
                                    <input
                                        className="form-control my-1"
                                        type="text"
                                        placeholder="Name"
                                        value={person.name}
                                        onChange={event => changePersonField(person.id, 'name', event.target.value)}
                                    />
                                    <input
                                        className="form-control my-1"
                                        type="number"
                                        placeholder="Sum"
                                        value={person.howMuch}
                                        onChange={event => changePersonPay(person.id, 'howMuch', event.target.value)}
                                    />
                                </div>
                                <div>
                                    <button
                                        className="btn btn-primary m-1"
                                        type="button"
                                        onClick={() => removePerson(person.id)}
                                    >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        ))
                    ) : (
                        <div>
                            <h4>Please add person</h4>
                        </div>
                    )}
                    <IndividualCounter people={people}/>
                </div>
            </form>
        </div>
    );
};

export default IndividualPay;