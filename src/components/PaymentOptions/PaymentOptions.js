import {useState} from 'react';
import IndividualPay from "../IndividualPay/IndividualPay";
import EquallyPay from "../EquallyPay/EquallyPay";


const PaymentOptions = () => {
    const [paymentCheck, setPaymentCheck] = useState('equally');

    const onRadioChange = e => {
        setPaymentCheck(e.target.value);
    };

    return (
        <div>
            <div className="form-check my-2">
                <input
                    className="form-check-input"
                    type="radio"
                    name="paymentOption"
                    value="equally"
                    checked={paymentCheck === 'equally'}
                    onChange={onRadioChange}
                />
                <label className="form-check-label">
                    Pay equally
                </label>
            </div>
            <div className="form-check my-2">
                <input
                    className="form-check-input"
                    type="radio"
                    name="paymentOption"
                    value="individual"
                    checked={paymentCheck === 'individual'}
                    onChange={onRadioChange}
                />
                    <label className="form-check-label">
                        Pay individual
                    </label>
            </div>
            {paymentCheck === 'individual' ? (<IndividualPay/>) : (<EquallyPay/>)}
        </div>
    );
};

export default PaymentOptions;