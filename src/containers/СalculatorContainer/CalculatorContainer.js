import React from 'react';
import '../../assets/bootstrap.min.css'
import PaymentOptions from "../../components/PaymentOptions/PaymentOptions";


const CalculatorContainer = () => {
    return (
        <div className="container p-5">
            <PaymentOptions/>
        </div>
    );
};

export default CalculatorContainer;